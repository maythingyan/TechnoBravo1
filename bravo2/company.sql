-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2018 at 08:18 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `company`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(10) NOT NULL,
  `name` varchar(225) NOT NULL,
  `category` varchar(225) NOT NULL,
  `address` varchar(225) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `image` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`, `category`, `address`, `phone`, `image`) VALUES
(1, 'Acer', 'computer', 'No.775,Maharbandoola Rd,Lanmadaw Township,Yangon,1131', '01227781', 'image/acer.jpeg'),
(2, 'Lenovo', 'computer', 'Mingarlar Mandalay,2nd floor', '09774705499', 'image/lenovo.jpg'),
(3, 'Dell', 'computer', 'Seikkanthar street,Yangon', '09441114411', 'image/dell.jpg'),
(4, 'Toshiba', 'computer', 'N0.168,83Streeet,Bet:24 Street and 25 Atreet,Aungmyaetharsan Township,Mandalay', '0231673', 'image/toshiba.jpg'),
(5, 'Sony', 'computer', 'N04-020,Junction City 4th floor,coner of BoGyoke road and Shwedagon Pagoda road,Pabetan Township,Yangon,11143', '09422999963', 'image/sony.jpg'),
(6, 'Asus', 'computer', 'Yangon', '092450980', 'image/asus.jpg'),
(7, 'Hp', 'computer', 'no(A-4),78Street Yangon-mandalay Road', '09777080010', 'image/hp.jpeg'),
(8, 'Samsung', 'computer', '73Street,between 28Street and 29Street,Mandalay ', '02-4039569', 'image/samsung.jpeg'),
(9, 'Iphone', 'phone', '78Street,between 27street and 28 street,mandalay', '09797788991', 'image/iphone.jpeg'),
(10, 'Samsung', 'phone', '73Streeet,between 28 street and 29 Street,Mandalay', '024039569', 'image/samsungp.jpeg'),
(11, 'Mi', 'phone', '78Street,28 streeet and 29 street,Mandalay', '09263333663', 'image/mi.jpeg'),
(12, 'Oppo', 'phone', '78Street,27 street and 28 street,Mandalay', '09797788991', 'image/oppo.jpeg'),
(13, 'Huawei', 'phone', '78Street,between 27 street and 28 street,Mandalay', '09797788991', 'image/huawei.jpeg'),
(14, 'Vivo', 'phone', '78Street,27 street and 28 street,Mandalay', '09797788991', 'image/vivo.jpeg'),
(15, 'Sony', 'phone', 'No.04-020,Junction City 4th floor,coner of BoGyoke road and Shwedagon Pagoda road,Pabetan Township,Yangon,11143', '09422999963', 'image/sonyp.jpeg'),
(16, 'Nokia', 'phone', '78Street,between 27 street and 28 street,Mandalay', '09797788991', 'imagee/nokia.jpeg'),
(17, 'Honor', 'phone', '78Street,between 27 street and 28 street,Mandalay', '09797788991', 'image/honor.jpeg'),
(18, 'Lg', 'phone', '78Street,between 27 street and 28 street,Mandalay', '09797788991', 'image/lg.jpeg'),
(19, 'Lenovo', 'phone', '78Street,between 27 street and 28 street,Mandalay', '09797788991', 'image/lenovop.jpeg'),
(20, 'Myanmar Build & Decor', 'decoration', 'Myanmar Event Part at Mindama road,Yangon', '09798579804', 'image/b&d.jpeg'),
(21, 'B2-Home', 'decoration', 'Kamar Yout Township,Yangon', '09976509143', 'image/b2home.jpeg'),
(22, 'Rave-Inetrior', 'decoration', 'Yangon', '09448014893', 'image/rave.jpeg'),
(23, 'Construction & Decoration', 'decoration', 'Mandalay', '09692695115', 'image/cad.jpeg'),
(24, 'Comet Desing & Decoration', 'decoration', '22A, Bo Yar Nyunt ,Dagon,Yangon', '01242502', 'image/comet.jpeg'),
(25, 'Decor & More', 'decoration', 'Pazundaung Tsp,Yangon 11171', '01200679', 'image/d&m.jpeg'),
(26, 'Door & Window(Myanmar)', 'decoration', 'No 326,Pyi Tharyar Street,Yangon', '09965516635', 'image/d&w.jpeg'),
(27, 'DMS Home Decoration', 'decoration', '68 Street and 69 Street,Tharyar Waddy Mingyi Road,Kywese Kan Car Parking,Mandalay', '09977936429', 'image/dms.jpeg'),
(28, 'Home Decoration', 'decoration', 'No 632,9th Street,Insein Township,Yangon', '09421112495', 'image/hd.jpeg'),
(29, 'KTM Home Decoration', 'decoration', 'Yangon', '09799555516', 'image/ktm.jpeg'),
(30, 'May Htike Wallpaper,Sofa & Home Decoration', 'decoration', 'No47,Thuwunna Park,Yangon', '09776239881 ', 'image/mh.jpeg'),
(31, 'Soco Home & Decoration', 'decoration', '55Street,38 street and 39 street ,Mandalay', '09250381318', 'image/soco.jpeg'),
(32, 'Top Myanmar Aluminium', 'decoration', '62 street,Between Gant Gaw st& Myawaddy Min Gyi Rd,Mandalay', '09400881230', 'image/tma.jpeg'),
(33, 'PKH Home decoration & Construction', 'decoration', 'No.136,Room-6,Thitsar Rd,YAnkin Township,Yangon', '09260667844', 'image/pkh.jpeg'),
(34, 'CET Construction & Enngeering Trading', 'decoration', 'Da(6/63),Shwe Kan Kaw lane,\r\nbetween 38&39,Ma ha Myaing,Mandalay', '092009596', 'image/cet.jpeg'),
(35, 'Three Swallows Construction', 'decoration', 'Parami Road,Yangon', '09423721546', 'image/tsc.jpeg'),
(36, 'EMC Construction', 'decoration', 'Chanayetharzan Township,Mandalay', '092029089', 'image/emc.jpeg'),
(37, 'Mayor COnstruction ', 'decoration', '83th street,between 25&26 Road,mandalay', '092006747', 'image/mc.jpeg'),
(38, 'Mandalay Consultant Group', 'decoration', '40th street,Bet 71&72 streets.,Mandalay', '09963523695', 'image/mcg.jpeg'),
(39, 'Amaxing Design & Construction', 'decoration', '80 Rd,Bet 37th&38st,Mandalay', '092060335', 'image/imazing.jpeg'),
(40, 'Volvo Car Showroom', 'Car', '146,Dhamazedi Road,bahan,Yangon', '09965086586', 'image/volvo.jpg'),
(41, 'Mazda Car Showroom', 'Car', 'No.96,Kabar Aye Bagoda Road,Sayar San Quarter,Yangon', '09260259991', 'image/mazda.jpg'),
(43, 'Sakura Auto Acyion Centre', 'Car', 'Cherry Garden City 14/3 WArd,South Okkalapa Township,Yangon', '018566109', 'image/sakura.jpg'),
(44, 'Mitsubishi Showroom', 'Car', 'No413,62nd Street Between 36th st & 37th,\r\nAmarapura', '09259100446', 'image/mitsubishi.jpeg'),
(45, 'Toyota', 'Car', 'Block 542,Upine-10/339,Mandalay-PyinOoLwin Road,35th&37th street,Aung Pin Lae,Mandalay', '09969700001', 'image/toyota.png'),
(46, 'Jagura Car Showroom', 'Car', 'No.3,Insein Road,Yangon', '019669034', 'image/ja.jpg'),
(47, 'Land Rover Car Showroom', 'Car', 'No.3,Insein Road,Yangon', '019669036', 'image/land.jpg'),
(48, 'KIA Car Showroom', 'Car', 'No.22,23-A,Yankin Township,Yangon', '09402726463', 'image/kia.jpg'),
(49, 'World Bank Myanmar', 'Bank', 'No.57,Pyay Road,Hlaing Township,Yangon', '01654824', 'image/world.jpeg'),
(50, 'Myanmar Microfinance Bank', 'Bank', 'Yangon', '012306290', 'image/mmb.jpeg'),
(51, 'KBZ Bank', 'Bank', '73Street ,between37 street and 38 Street,Mandalay', '09454825067', 'image/kbz.jpg'),
(52, 'CB Bank', 'Bank', 'No46,Botahaung Township,Yangon', '012317770', 'image/cb.jpeg'),
(53, 'Myanmar Apex Bank', 'Bank', 'No207,Botahtaung Township,Yangon', '018398811', 'image/mab.jpeg'),
(54, 'Yoma Bank', 'Bank', '192,8th floor,Myanmar Plaza,Bahan Township,Yangon', '09796629662', 'image/yoma.jpeg'),
(55, 'AYA Bank', 'Bank', '416,Mshabandoola Road,Kyauktada Towndhip,Yangon', '09420774137', 'image/aya.jpeg'),
(56, 'United Amara Bank', 'Bank', 'Yangon', '018551335', 'image/uab.jpeg'),
(57, 'Chid Bank', 'Bank', 'N0 60,Shwedagon Pagoda Road, Dagon Township,Yangon', '09771049627', 'image/chid.jpeg'),
(58, 'Myanmar Citizens Bank', 'Bank', 'Yangon', '01255944', 'image/mcb.jpeg'),
(59, 'Central Bank of Myanmar', 'Bank', ' Office 55,Naypyitaw ', '09797788991', 'image/baho.jpeg'),
(60, 'Shwe Bank', 'Bank', 'Coner of Pansoedan Road,Yangon', '012306978', 'image/shwe.jpeg'),
(61, 'MOB Bank', 'Bank', 'Yangon', '01246594', 'image/mob.jpeg'),
(62, 'Sagacious Myanmar Travels & Tours', 'travels & tours', 'Pyi Gyi Ta Gon Township,Mandalay', '09402502155', 'image/smt.jpeg'),
(63, 'D&T Myanmar Travels', 'travels & tours', 'No.25/55,Aungmyaetharsan ,Mandalay', '09402775773', 'image/d&t.jpeg'),
(64, 'Sabai Travels&Tours', 'travels & tours', '78Street,Mandalay', '0991039018', 'image/sabai.jpeg'),
(65, 'Princess Amara', 'travels & tours', '64th St,Mandalay', '09973388200', 'image/prince.jpeg'),
(66, 'Heritage Mandalay', 'travels & tours', 'No.004/92,27th Street ,Mandalay', '09421911919', 'image/heritage.jpeg'),
(67, 'LM Myanmar', 'travels & tours', 'No.25,between 47 street and 48 street ,Mandalay', '09421723682', 'image/lm.jpeg'),
(68, 'Zone Express Tours', 'travels & tours', '68 Street,26th & 27th Street,Mandalay', '022844651', 'image/zone.jpeg'),
(69, 'Interesting Myanmar', 'travels & tours', '37th Street,Mandalay', '0978009977', 'image/interest.jpeg'),
(70, 'Mandalay In Bloom Travels & Tours', 'travels & tours', '58th Street ,between 40th& 41th,Mandalay', '09797480217', 'image/bloom.jpeg'),
(71, 'Hotel Mandalay', 'Hotel', '78th Street,Mandalay', '0271582', 'image/mandalay.jpeg'),
(72, 'Tiger One Hotel', 'Hotel', '31th Street,between 76th& 77th,Mandalay', '0274239', 'image/tiger.jpeg'),
(73, 'Bagan King Hotel', 'Hotel', '73th Street,between 28th& 29th Street,Mandalay', '022847471', 'image/bagan.jpeg'),
(74, 'Living Hotel', 'Hotel', '79th Street ,Mandalay', '0232277', 'image/living.jpeg'),
(75, 'Royal Diamond Hotel', 'Hotel', '77th Street,between 32th & 33th Street,Mandalay', '00449995630', 'image/royal.jpeg'),
(76, 'Hotel Apex Mandalay', 'Hotel', '35th Street,Chan Aye Thar Zan Township,Mandalay', '09259090707', 'image/apex.jpeg'),
(85, 'Ford Myanmar Showroom', 'Car', 'Yangon', '01521959', 'image/ford.JPG'),
(86, 'Honda Car Showroom', 'Car', '35street,bet:71*72street;MaharAungMyay Tsp,Mandalay', '09400085580', 'image/honda.jpeg'),
(87, 'Farmer Auto Car Showroom', 'Car', 'NO.3,Yazahtarni Road,NayPyiTaw.', '0931589643', 'image/farmer.jpeg'),
(88, 'Myanmar Car Pro', 'Car', 'NO.332,First Floor,bet:23st & 24st,Strand Road,Yangon.', '095031480', 'image/pro.jpeg'),
(89, 'UD Group', 'Car', 'ChanMyaTharSi,Mandalay', '0901001111', 'image/ud.jpeg'),
(90, 'Myanmar Car Dealer', 'Car', 'No1,B-2.8F.Moe Kaung Rd.Yankin Tsp Yangon.', '09777770097', 'image/mcd.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
